#
# nodes - ops - signoz-poc-eks
#

resource "aws_security_group" "signoz-poc-eks-node-access" {
  name        = "signoz-poc-eks-node-access"
  description = "Allows ssh access to signoz eks nodes"
  vpc_id      = data.aws_vpc.signoz-poc-vpc.id

  tags = {
    Name = "signoz-poc-eks-node-access"
  }
}

resource "aws_vpc_security_group_ingress_rule" "signoz-eks-node-ssh" {
  description                  = "Allows ssh access to signoz eks nodes from bastion"
  from_port                    = 22
  to_port                      = 22
  ip_protocol                  = "tcp"
  security_group_id            = aws_security_group.signoz-poc-eks-node-access.id
  referenced_security_group_id = aws_security_group.signoz-poc-eks-bastion.id
}


# TODO: custom-track ami versions
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_node_group#tracking-the-latest-eks-node-group-ami-releases

resource "aws_eks_node_group" "signoz-poc-nodegroup" {
  cluster_name    = aws_eks_cluster.signoz-poc.name
  node_group_name = "signoz-poc-node-group"
  node_role_arn   = aws_iam_role.eks-node.arn
  subnet_ids      = data.aws_subnets.private.ids
  capacity_type   = "ON_DEMAND"
  disk_size       = "30"
  instance_types = [
    "t3.medium",
    "t3.large",
  ]

  remote_access {
    ec2_ssh_key               = "common-aws"
    source_security_group_ids = [aws_security_group.signoz-poc-eks-bastion.id]
  }

  labels = tomap({ env = "poc" })

  scaling_config {
    desired_size = 2
    max_size     = 4
    min_size     = 1
  }

  # TODO: implement custom launch template for tagging nodes
  #launch_template {
  #  id      = aws_launch_template.signoz-poc-eks-node-tags.id
  #  version = 1
  #}

  update_config {
    max_unavailable = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.eks-node-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.eks-node-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.eks-node-AmazonEKS_CNI_IPv6_Policy,
    aws_iam_role_policy_attachment.eks-node-AmazonEBSCSIDriverPolicy,
    aws_iam_role_policy_attachment.eks-node-AmazonEC2ContainerRegistryReadOnly,
  ]

  # Optional: Allow external changes without Terraform plan difference
  lifecycle {
    ignore_changes = [scaling_config[0].desired_size]
  }
}
