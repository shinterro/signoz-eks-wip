#
# variables - ops - signoz-poc-eks
#

# target region
variable "aws_region" {
  default = "us-east-2"
}

#
# target vpc lookups
#

data "aws_vpc" "signoz-poc-vpc" {
  filter {
    name   = "tag:Name"
    values = ["signoz-poc-vpc"]
  }
}

# identifies all subnets tagged with Tier "private"
data "aws_subnets" "private" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.signoz-poc-vpc.id]
  }

  tags = {
    Tier = "private"
  }
}

# identifies all subnets tagged with Tier "public"
data "aws_subnets" "public" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.signoz-poc-vpc.id]
  }

  tags = {
    Tier = "public"
  }
}

locals {
  cluster_name = "signoz-poc"
}
