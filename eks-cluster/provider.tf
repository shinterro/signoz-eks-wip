#
# provider - ops - signoz-poc-eks
#

provider "aws" {
  region = var.aws_region
  default_tags {
    tags = {
      Managed     = "terraform"
      Project     = "ga-signoz-poc"
      Owner       = "TechOps"
      Maintainer  = "TechOps"
      Environment = "poc"
      Service     = "Signoz"
      Application = "eks"
    }
  }
}

terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.37.0"
    }
    template = {
      source  = "hashicorp/template"
      version = "~> 2.2"
    }
  }

  backend "s3" {
    bucket         = "ga-terraform"
    key            = "ga-signoz-poc-eks-ops.tfstate"
    region         = "us-east-2"
    dynamodb_table = "ga-terraform-locks-common"
    encrypt        = true
  }

  required_version = ">= 1.4.0"
}
