#
# iam roles - ops - signoz-poc-eks
#

## Cluster Permissions ##

# trust relationship for eks role assumption
data "aws_iam_policy_document" "eks-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "eks-cluster" {
  name               = "eks-${local.cluster_name}-cluster"
  assume_role_policy = data.aws_iam_policy_document.eks-assume-role-policy.json
}

resource "aws_iam_role_policy_attachment" "eks-cluster-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks-cluster.name
}

resource "aws_iam_role_policy_attachment" "eks-cluster-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.eks-cluster.name
}

resource "aws_iam_role_policy_attachment" "eks-cluster-AmazonEKSVPCResourceController" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.eks-cluster.name
}

## Worker Node Permissions ##

# trust relationship for ec2 instance role assumption
data "aws_iam_policy_document" "instance-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "eks-node" {
  name               = "eks-${local.cluster_name}-node"
  assume_role_policy = data.aws_iam_policy_document.instance-assume-role-policy.json
}

resource "aws_iam_role_policy_attachment" "eks-node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eks-node.name
}

resource "aws_iam_role_policy_attachment" "eks-node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eks-node.name
}

resource "aws_iam_role_policy_attachment" "eks-node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks-node.name
}

resource "aws_iam_role_policy_attachment" "eks-node-CloudWatchReadOnlyAccess" {
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchReadOnlyAccess"
  role       = aws_iam_role.eks-node.name
}

resource "aws_iam_role_policy_attachment" "AmazonSSMManagedInstanceCore" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  role       = aws_iam_role.eks-node.name
}

# CSI Driver permissions to allow node management of EBS volumes
# https://github.com/kubernetes-sigs/aws-ebs-csi-driver/blob/master/docs/install.md
resource "aws_iam_role_policy_attachment" "AmazonEBSCSIDriverPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
  role       = aws_iam_role.eks-node.name
}


## AWS CNI Add-On Requires a specific IPv6 Policy ##
# https://docs.aws.amazon.com/eks/latest/userguide/cni-iam-role.html#cni-iam-role-create-ipv6-policy
# https://docs.aws.amazon.com/eks/latest/userguide/cni-ipv6.html

data "aws_iam_policy_document" "eks-node-AmazonEKS_CNI_IPv6_Policy" {
  statement {
    sid    = "AmazonEKSCNIIPv6PolicyAssign0"
    effect = "Allow"
    actions = [
      "ec2:AssignIpv6Addresses",
      "ec2:DescribeInstances",
      "ec2:DescribeTags",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DescribeInstanceTypes"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    sid    = "AmazonEKSCNIIPv6PolicyCreateTags0"
    effect = "Allow"
    actions = [
      "ec2:CreateTags"
    ]
    resources = [
      "arn:aws:ec2:*:*:network-interface/*"
    ]
  }
}

resource "aws_iam_policy" "eks-node-AmazonEKS_CNI_IPv6_Policy" {
  name        = "Amazon_EKS_CNI_IPv6_Policy"
  path        = "/service/"
  description = "Policy to support eks CNI IPv6 networking mechanics"
  policy      = data.aws_iam_policy_document.eks-node-AmazonEKS_CNI_IPv6_Policy.json
}

resource "aws_iam_role_policy_attachment" "eks-node-AmazonEKS_CNI_IPv6_Policy" {
  policy_arn = aws_iam_policy.eks-node-AmazonEKS_CNI_IPv6_Policy.arn
  role       = aws_iam_role.eks-node.name
}


## custom autoscaling policy ##

data "aws_iam_policy_document" "eks-node-autoscaling" {
  statement {
    sid    = "EKSNodeAutoscalingPolicy0"
    effect = "Allow"
    actions = [
      "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeAutoScalingInstances",
      "autoscaling:DescribeTags",
      "autoscaling:DescribeLaunchConfigurations",
      "autoscaling:SetDesiredCapacity",
      "autoscaling:TerminateInstanceInAutoScalingGroup",
      "ec2:DescribeLaunchTemplateVersions",
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "eks-node-autoscaling" {
  name        = "eks-node-autoscaling"
  path        = "/service/"
  description = "template to support eks node autoscaling"
  policy      = data.aws_iam_policy_document.eks-node-autoscaling.json
}

resource "aws_iam_role_policy_attachment" "eks-node-autoscaling" {
  policy_arn = aws_iam_policy.eks-node-autoscaling.arn
  role       = aws_iam_role.eks-node.name
}

## node instance profile ##

resource "aws_iam_instance_profile" "eks-node" {
  depends_on = [aws_iam_role.eks-node]
  name       = "eks-${local.cluster_name}-node"
  role       = aws_iam_role.eks-node.name
}

