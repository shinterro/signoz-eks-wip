#
# subnets - ops - signoz-poc
#

resource "aws_subnet" "signoz-poc-vpc-public-a" {
  vpc_id          = aws_vpc.signoz-poc.id
  ipv6_cidr_block = cidrsubnet(aws_vpc.signoz-poc.ipv6_cidr_block, 8, 0)
  cidr_block      = cidrsubnet(aws_vpc.signoz-poc.cidr_block, 4, 10)

  ipv6_native                                    = false
  assign_ipv6_address_on_creation                = true
  enable_resource_name_dns_aaaa_record_on_launch = true

  availability_zone_id = data.aws_availability_zones.available.zone_ids[0]
  tags = {
    Name = "signoz-poc-vpc-public-a"
    Tier = "public"
  }
}

resource "aws_subnet" "signoz-poc-vpc-public-b" {
  vpc_id          = aws_vpc.signoz-poc.id
  ipv6_cidr_block = cidrsubnet(aws_vpc.signoz-poc.ipv6_cidr_block, 8, 1)
  cidr_block      = cidrsubnet(aws_vpc.signoz-poc.cidr_block, 4, 11)

  ipv6_native                                    = false
  assign_ipv6_address_on_creation                = true
  enable_resource_name_dns_aaaa_record_on_launch = true

  availability_zone_id = data.aws_availability_zones.available.zone_ids[1]
  tags = {
    Name = "signoz-poc-vpc-public-b"
    Tier = "public"
  }
}


resource "aws_subnet" "signoz-poc-vpc-private-a" {
  vpc_id          = aws_vpc.signoz-poc.id
  ipv6_cidr_block = cidrsubnet(aws_vpc.signoz-poc.ipv6_cidr_block, 8, 2)
  cidr_block      = cidrsubnet(aws_vpc.signoz-poc.cidr_block, 4, 12)

  ipv6_native                                    = false
  assign_ipv6_address_on_creation                = true
  enable_resource_name_dns_aaaa_record_on_launch = true

  availability_zone_id = data.aws_availability_zones.available.zone_ids[0]
  tags = {
    Name = "signoz-poc-vpc-private-a"
    Tier = "private"
  }
}

resource "aws_subnet" "signoz-poc-vpc-private-b" {
  vpc_id          = aws_vpc.signoz-poc.id
  ipv6_cidr_block = cidrsubnet(aws_vpc.signoz-poc.ipv6_cidr_block, 8, 3)
  cidr_block      = cidrsubnet(aws_vpc.signoz-poc.cidr_block, 4, 13)

  ipv6_native                                    = false
  assign_ipv6_address_on_creation                = true
  enable_resource_name_dns_aaaa_record_on_launch = true

  availability_zone_id = data.aws_availability_zones.available.zone_ids[1]
  tags = {
    Name = "signoz-poc-vpc-private-b"
    Tier = "private"
  }
}

