#
# routes: private - ops - signoz poc
#

## nat instances for public subnets
# https://github.com/RaJiska/terraform-aws-fck-nat

module "signoz-public-nat-a" {
  source  = "RaJiska/fck-nat/aws"
  version = "1.2.0"

  name      = "signoz-poc-nat-a"
  vpc_id    = aws_vpc.signoz-poc.id
  subnet_id = aws_subnet.signoz-poc-vpc-public-a.id
}

module "signoz-public-nat-b" {
  source  = "RaJiska/fck-nat/aws"
  version = "1.2.0"

  name      = "signoz-poc-nat-b"
  vpc_id    = aws_vpc.signoz-poc.id
  subnet_id = aws_subnet.signoz-poc-vpc-public-b.id
}

# ipv6 egress-only igw

resource "aws_egress_only_internet_gateway" "signoz-poc-ipv6-egress-igw" {
  vpc_id = aws_vpc.signoz-poc.id

  tags = {
    Name = "signoz-poc-ipv6-egress-igw"
  }
}

# route tables

resource "aws_route_table" "signoz-poc-private-route-a" {
  vpc_id = aws_vpc.signoz-poc.id

  tags = {
    Name = "signoz-poc-private-route-a"
  }
}

resource "aws_route_table" "signoz-poc-private-route-b" {
  vpc_id = aws_vpc.signoz-poc.id

  tags = {
    Name = "signoz-poc-private-route-b"
  }
}

# nat instance routes (ipv4)

resource "aws_route" "signoz-poc-private-egress-a" {
  route_table_id         = aws_route_table.signoz-poc-private-route-a.id
  destination_cidr_block = "0.0.0.0/0"
  network_interface_id   = module.signoz-public-nat-a.eni_id
}

resource "aws_route" "signoz-poc-private-egress-b" {
  route_table_id         = aws_route_table.signoz-poc-private-route-b.id
  destination_cidr_block = "0.0.0.0/0"
  network_interface_id   = module.signoz-public-nat-b.eni_id
}

# egress routes (ipv6)

resource "aws_route" "signoz-poc-private-egress-a-ipv6" {
  route_table_id              = aws_route_table.signoz-poc-private-route-a.id
  destination_ipv6_cidr_block = "::/0"
  egress_only_gateway_id      = aws_egress_only_internet_gateway.signoz-poc-ipv6-egress-igw.id
}

resource "aws_route" "signoz-poc-private-egress-b-ipv6" {
  route_table_id              = aws_route_table.signoz-poc-private-route-b.id
  destination_ipv6_cidr_block = "::/0"
  egress_only_gateway_id      = aws_egress_only_internet_gateway.signoz-poc-ipv6-egress-igw.id
}

# route table associations

resource "aws_route_table_association" "signoz-poc-vpc-private-a" {
  subnet_id      = aws_subnet.signoz-poc-vpc-private-a.id
  route_table_id = aws_route_table.signoz-poc-private-route-a.id
}

resource "aws_route_table_association" "signoz-poc-vpc-private-b" {
  subnet_id      = aws_subnet.signoz-poc-vpc-private-b.id
  route_table_id = aws_route_table.signoz-poc-private-route-b.id
}
