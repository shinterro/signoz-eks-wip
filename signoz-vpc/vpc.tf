#
# vpc - ops - signoz-poc
#

# https://docs.aws.amazon.com/eks/latest/userguide/network_reqs.html

resource "aws_vpc" "signoz-poc" {
  cidr_block                       = var.vpc_cidr
  assign_generated_ipv6_cidr_block = true
  enable_dns_hostnames             = true
  enable_dns_support               = true

  tags = {
    "Name" = "signoz-poc-vpc"
  }
}
