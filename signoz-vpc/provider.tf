#
# provider - ops - signoz-poc
#

provider "aws" {
  region = var.aws_region
  default_tags {
    tags = {
      Managed     = "terraform"
      Project     = "ga-signoz-poc"
      Owner       = "Ops"
      Maintainer  = "Ops"
      Environment = "poc"
      Service     = "signoz"
      Application = "vpc"
    }
  }
}

terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.31.0"
    }
  }

  backend "s3" {
    bucket         = "ga-terraform"
    key            = "ga-signoz-poc-vpc.tfstate"
    region         = "us-east-2"
    dynamodb_table = "ga-terraform-locks-common"
    encrypt        = true
  }

  required_version = ">= 1.4.0"
}
