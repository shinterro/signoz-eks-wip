#
# variables - ops - signoz-poc
#

# target region
variable "aws_region" {
  default = "us-east-2"
}

# returns regionally-available supported AZ IDs
# aws ec2 describe-availability-zones --region <region>
# https://docs.aws.amazon.com/workspaces/latest/adminguide/azs-workspaces.html
data "aws_availability_zones" "available" {
  state = "available"
}

variable "vpc_cidr" {
  default = "172.14.0.0/16"
}
