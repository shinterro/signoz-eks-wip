#
# routes: public - ops - signoz poc
#


resource "aws_internet_gateway" "signoz-poc-igw" {
  vpc_id = aws_vpc.signoz-poc.id

  tags = {
    Name = "signoz-poc-igw"
  }
}


resource "aws_route_table" "signoz-poc-public-route" {
  vpc_id = aws_vpc.signoz-poc.id

  tags = {
    Name = "signoz-poc-public-route"
  }
}


resource "aws_route" "signoz-poc-route" {
  route_table_id         = aws_route_table.signoz-poc-public-route.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.signoz-poc-igw.id
}

resource "aws_route" "signoz-poc-route-ipv6" {
  route_table_id              = aws_route_table.signoz-poc-public-route.id
  destination_ipv6_cidr_block = "::/0"
  gateway_id                  = aws_internet_gateway.signoz-poc-igw.id
}


resource "aws_route_table_association" "signoz-poc-vpc-public-a" {
  subnet_id      = aws_subnet.signoz-poc-vpc-public-a.id
  route_table_id = aws_route_table.signoz-poc-public-route.id
}

resource "aws_route_table_association" "signoz-poc-vpc-public-b" {
  subnet_id      = aws_subnet.signoz-poc-vpc-public-b.id
  route_table_id = aws_route_table.signoz-poc-public-route.id
}

